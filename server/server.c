#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define SERV_PORT 9877
#define MAXLINE 4096



int main() {
    struct sockaddr_in servaddr;
    struct sockaddr_in cliaddr;
    int length = sizeof(cliaddr);
    char mesg[MAXLINE];
    char sendline[MAXLINE];
    memset (&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;servaddr.sin_addr.s_addr = htonl (INADDR_ANY);
    servaddr.sin_port = htons (SERV_PORT);
    int sockfd = socket(AF_INET,SOCK_DGRAM,0);
    bind (sockfd, (struct sockaddr *) &servaddr, sizeof (servaddr));
    while(1) {
        memset(mesg,0,sizeof(mesg));
        memset(sendline,0,sizeof(sendline));
        recvfrom(sockfd, mesg, MAXLINE, 0, (struct sockaddr *) &cliaddr, &length);
        for(int i =0;i<sizeof(mesg);i++){
            sendline[i]=toupper(mesg[i]);
        }
        sendto(sockfd, sendline, strlen(sendline), 0, (struct sockaddr *) &cliaddr, sizeof(cliaddr));
        printf("Received:%s\n", mesg);
        fflush(stdout);
    }
}